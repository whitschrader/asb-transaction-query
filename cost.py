import sys

if len(sys.argv) != 2:
	print "You need to specify a string to search for."
	sys.exit(1)
	
query = sys.argv[1]

#print query

f = open('export.csv')
cost = 0.00
x = 0
for line in f:
	fields = line.split(',')
	if len(fields) == 7:
		if query in fields[4] or query.upper() in fields[4].upper() or query.lower() in fields[4].lower() or \
			query in fields[5] or query.upper() in fields[5].upper() or query.lower() in fields[5].lower():
			v = float(fields[6])
			if v < 0:
				cost += v * -1	
f.close()

print 'Total amount spent for ' + query + ': ',
print '$' + str(cost)