asb-transaction-query
=====================

Scrapes ASB bank CSV transactions and lets you see how much you spent at a certain place.

cost.py: lets you query your bank statements with a string, and total up all transactions 
matching that particular string.

most-cost.py: lists in ascending order (from lowest to highest) where you've spent your
money the most.
