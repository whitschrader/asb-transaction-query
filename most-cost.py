f = open('export.csv')

hm = dict()
x = 1

for line in f:
	fields = line.split(',')
	if len(fields) == 7:
		
		# minor annoyance to account for...
		if x == 7:
			continue
			
		cost = float(fields[6]) 
		key = fields[4].replace('"','')
			
		if cost < 0:	
			if key not in hm.keys():
				hm[ key ] = cost * -1
				
			else:
				current_cost = hm[ key ]
				hm[ key ] = current_cost + (cost * -1)
			
	x += 1
f.close()

sorted_contents = sorted(hm, key=hm.get)
for s in sorted_contents:
	print s + ':  $' + str(hm[s])
