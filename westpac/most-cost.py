#Author chris Beckham, amended by Nathan Robb
#Westpac

import sys

f = open('export.csv')

hm = dict()
first_time = True

for line in f:

	if first_time:
		first_time = False
		continue

	fields = line.split(',')

	#if len(fields) > 6:

	cost = float(fields[1]) 
	key = fields[2].replace('"','')

	if cost < 0:	
		if key not in hm.keys():
			hm[ key ] = float(fields[1]) * -1

		else:
			current_cost = hm[ key ]
			hm[ key ] = current_cost + (cost * -1)

f.close()

total = 0

sorted_contents = sorted(hm, key=hm.get)
for s in sorted_contents:
	print s + ':  $' + str(hm[s])
	total += hm[s]

print "Total money spent: $" + str(total)

